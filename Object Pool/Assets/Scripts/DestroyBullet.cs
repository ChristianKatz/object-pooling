﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerfReq_Pres
{

    public class DestroyBullet : MonoBehaviour
    {

       void OnEnable()
        {
            Invoke("SetBulletOff", 2f);
        }
        private void OnDisable()
        {
            CancelInvoke();
        }
       void SetBulletOff()
        {
            gameObject.SetActive(false);
        }
    }
}
