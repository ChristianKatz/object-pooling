﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerfReq_Pres
{
    public class MoveBullet : MonoBehaviour
    {
        public float speed = 5;

        private void Update()
        {
            transform.Translate(0, speed * Time.deltaTime, 0);
        }

    }
}
