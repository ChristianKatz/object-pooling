﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PerfReq_Pres
{

    public class FireBullet : MonoBehaviour
    {
        public float fireTime = 0.05f;
        public float firstFireActivationTime = 0.5f;
        public GameObject bullet;

        public int pooledAmount = 20;
        List<GameObject> bullets;

        void Start()
        {
            bullets = new List<GameObject>();
            for(int i = 0; i < pooledAmount; i++)
            {
                GameObject obj = Instantiate(bullet);
                obj.SetActive(false);
                bullets.Add(obj);
            }

            InvokeRepeating("Fire", firstFireActivationTime , fireTime);
        }

        void Fire()
        {
            for(int i = 0; i < bullets.Count; i++)
            {
                if(!bullets[i].activeInHierarchy)
                {
                    bullets[i].transform.position = transform.position;
                    bullets[i].transform.rotation = transform.rotation;
                    bullets[i].SetActive(true);
                    break;
                }
            }
        }
    }
}
